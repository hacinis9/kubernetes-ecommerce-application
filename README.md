# SAE5.DevCloud.03 - Orchestrer une application conteneurisée
This is a project to create a product sales site.
The infrastructure for this project is a containerized infrastructure, orchestrated by **Kubernetes**.  
The infrastructure is hosted on the **OpenStack** server "rt.iut".

The network diagram of the infrastructure is as follows: 

<img src="./img/Schema_Reseau_SAE5dc03.jpg" alt="Network_Scheme">

## Getting Started
The application is divided into numerous services that communicate with each other and enable 
the user to purchase products. There are also 3 persistent databases running on an NFS service.

The services are as follows: 


<img src="./img/Service_scheme.png" alt="Service_Scheme">



These services are developed in Python. The APIs are based on the **Flask** library. 


## Usage

Connect to the frontend, which looks like this:

<img src="./img/FrontEnd.png" alt="FrontEnd">

You can then navigate through the site and add products to your cart.
Then you can go to the cart and pay for your order.
An Email will be sent to you with the details of your order.

All the APIs are sending metrics of their activity to **Prometheus** on the route /metrics.


## Versioning and images

We use `https://gricad-gitlab.univ-grenoble-alpes.fr/` for versioning. 

Images of the services are available on the registry `gricad-registry.univ-grenoble-alpes.fr`

## Authors
- <a href="https://sofianehacini.eu/">**Sofiane HACINI**</a>
- **Joshua KEELY**</a>
- **Lucas PECOUT**</a>
- **Jilian MAUREL**</a>
- **Iwen KERADEC-DUHARD**</a>

<img src="./img/IMG_3494.webp" alt="Team">

This project achieved success and was showcased to our IT degree instructors as a final semester project presentation. We were honored with a score of 18 out of 20. Below is our PowerPoint presentation (in French).

[![Open PowerPoint Presentation](https://img.shields.io/badge/Open-PowerPoint%20Presentation-blue?style=for-the-badge&logo=microsoftoffice)](https://gitlab.com/hacinis9/kubernetes-ecommerce-application/-/blob/main/SAE-SOUTENANCE.pptx?ref_type=heads)

