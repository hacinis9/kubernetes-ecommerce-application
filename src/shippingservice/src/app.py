import pynats2
import threading
import os
import json
import logging

# Récupérer les variables d'environnement
NATS_HOST = os.environ["NATS_HOST"]
NATS_PORT = os.environ["NATS_PORT"]
event = threading.Event()  # Créer un event

logger = logging.getLogger("shippingservice")
logging.basicConfig(level=logging.DEBUG)

logger.info(f'NATS_HOST : {NATS_HOST}')
logger.info(f'NATS_PORT : {NATS_PORT}')


def callback(msg):
    # Get message and convert to dict
    dixo = msg.payload.decode("utf-8")
    dixo = json.loads(dixo)
    # Afficher le message reçu
    logger.info(
        f"Sending package with {dixo['total_items']}  items to {dixo['customer']['name']} at {dixo['customer']['address']} , {dixo['customer']['postalzip']} {dixo['customer']['city']}")


# Connection to server NATS and subscribe to subject "order"
def wait_chat_message():
    try:
        with pynats2.NATSClient(f"nats://{NATS_HOST}:{NATS_PORT}") as client:
            client.connect()
            client.subscribe(subject="order", callback=callback)
            client.publish(subject="order", payload=b"Start server!")
            event.wait()
    except Exception as e:
        logger.error(e)


if __name__ == '__main__':
    wait_chat_message()
