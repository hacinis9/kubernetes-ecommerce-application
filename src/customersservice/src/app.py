import flask
import flask_restx
from prometheus_client import Counter, generate_latest
import db
import os
import flask.scaffold


app = flask.Flask(__name__)

# Authorization model for swagger
authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}

api = flask_restx.Api(app=app, doc='/doc',
                      title='API for customers service', description='API for customers service',
                      version="0.1", authorizations=authorizations, security='apikey', namespace="customers_service")

# ENVIRONMENT VARIABLES
API_KEY = os.environ["API_KEY"]
LOG_LEVEL = os.environ["LOG_LEVEL"]

# Logging
app.logger.setLevel(LOG_LEVEL)

app.logger.info(f'\nAPI_KEY: {API_KEY}, \n'
                f'LOG_LEVEL: {LOG_LEVEL},\n'
                f'POSTGRES_DB : {db.POSTGRES_DB},\n'
                f'POSTGRES_HOST : {db.POSTGRES_HOST},\n'
                f'POSTGRES_PORT : {db.POSTGRES_PORT},\n'
                f'POSTGRES_USER : {db.POSTGRES_USER},\n'
                f'POSTGRES_PASSWORD : {db.POSTGRES_PASSWORD},\n')

# Metrics
user_failure_request_count = Counter('customersservice_counter_failure_request', 'User failure request count')
user_request_count = Counter('customersservice_counter_success_request', 'User request count', ["cid"])


# Check API KEY
def api_key_required(func):
    """Decorator to check for API key

    :param func: The function to decorate
    :return: The decorated function
    """

    def wrapper(*args, **kwargs):
        key = flask.request.headers.get('X-API-KEY')

        if key == API_KEY:
            return func(*args, **kwargs)
        else:
            app.logger.error(f'Incorrect or missing API key')
            flask.abort(401, "Incorrect or missing API key")

    return wrapper


# API models
customer_data_model = api.model('customer_data', {
    'id': flask_restx.fields.Integer,
    'name': flask_restx.fields.String,
    'phone': flask_restx.fields.String,
    'email': flask_restx.fields.String,
    'address': flask_restx.fields.String,
    'city': flask_restx.fields.String,
    'postalzip': flask_restx.fields.String,
    'password': flask_restx.fields.String
})


# ------------------


@api.param('customer_id', 'The customer identifier')
@api.route('/api/customer/<string:customer_id>')
class Customer(flask_restx.Resource):
    """Customer Resource"""

    @api.doc(security='apikey')
    @api.response(404, 'Customer not found')
    @api.response(200, "Ok", customer_data_model)
    @api.response(401, 'Incorrect or missing API key')
    @api_key_required
    def get(self, customer_id):
        """
        GET response

        :param customer_id: The customer identifier
        :returns The customer data
        """

        # Connection to the database and request

        bdd = db.get_db()
        cursor = bdd.cursor()
        cursor.execute("SELECT * FROM customers WHERE customers.id = %(customer_id)s",
                       {'customer_id': customer_id})
        row = cursor.fetchone()
        app.logger.debug(f'Customer information: {row}')

        if row is None:
            app.logger.warning(f'Customer {customer_id} not found in the database')
            user_failure_request_count.inc()
            flask_restx.abort(404, f"Customer {customer_id} not found in the database")
        db.close_db()

        # Increments metrics
        user_request_count.labels(customer_id).inc()

        # Convert data to follow the scheme
        return db.to_json_data(row), 200


@api.param('customer_email', 'The customer email')
@api.route('/api/customer/email/<string:customer_email>')
class CustomerEmail(flask_restx.Resource):
    """CustomerEmail Resource"""

    @api.doc(security='apikey')
    @api.response(404, 'Customer not found')
    @api.response(401, 'Incorrect or missing API key')
    @api.response(200, "Ok", customer_data_model)
    @api_key_required
    def get(self, customer_email):
        """
        GET response

        :param customer_email: The customer email
        :return: The customer data
        """

        # Connection to the database and request
        bdd = db.get_db()
        cursor = bdd.cursor()
        cursor.execute("SELECT * FROM customers WHERE customers.email = %(customer_email)s",
                       {'customer_email': customer_email})
        row = cursor.fetchone()
        app.logger.debug(f'Customer information: {row}')

        if row is None:
            flask.current_app.logger.warning(f'Customer {customer_email} not found in the database')
            user_failure_request_count.inc()
            flask_restx.abort(404, f"Customer {customer_email} not found in the database")
        db.close_db()

        # Increments metrics
        user_request_count.labels(db.to_json_data(row)["id"]).inc()

        # Convert data to follow the scheme
        return db.to_json_data(row), 200


@app.route('/api/customer/metrics')
def metrics():
    """Metrics endpoint"""
    return generate_latest(), 200


if __name__ == '__main__':
    app.run(debug=True, port=5000, host="0.0.0.0")
