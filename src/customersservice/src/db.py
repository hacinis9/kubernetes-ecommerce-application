import flask
import psycopg2
from flask import current_app
import os
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

# VARIABLES
POSTGRES_HOST = os.environ.get("POSTGRES_HOST")
POSTGRES_PORT = os.environ.get("POSTGRES_PORT")
POSTGRES_DB = os.environ.get("POSTGRES_DB")
POSTGRES_PASSWORD = os.environ.get("POSTGRES_PASSWORD")
POSTGRES_USER = os.environ.get("POSTGRES_USER")

logger.debug(f'POSTGRES_HOST : {POSTGRES_HOST}')
logger.debug(f'POSTGRES_PORT : {POSTGRES_PORT}')
logger.debug(f'POSTGRES_DB : {POSTGRES_DB}')
logger.debug(f'POSTGRES_PASSWORD : {POSTGRES_PASSWORD}')
logger.debug(f'POSTGRES_USER : {POSTGRES_USER}')


def get_db():
    """Get the database connection"""
    if 'db' not in flask.g:
        flask.g.db = psycopg2.connect(host=POSTGRES_HOST,
                                      port=POSTGRES_PORT,
                                      database=POSTGRES_DB,
                                      user=POSTGRES_USER,
                                      password=POSTGRES_PASSWORD)
        current_app.logger.info(f'Connected to the database')
    return flask.g.db


def close_db(e=None):
    db = flask.g.pop('db', None)
    if db is not None:
        db.close()
        current_app.logger.info(f'Déconnexion de la BDD')


def to_json_data(row):
    json_data = {'id': row[0],
                 'name': row[1],
                 'phone': row[2],
                 'email': row[3],
                 'address': row[4],
                 'city': row[5],
                 'postalzip': row[6],
                 'password': row[7]
                 }
    return json_data
