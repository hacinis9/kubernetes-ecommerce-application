import logging

import flask
import flask_restx
from prometheus_client import Counter, generate_latest
import os
import requests
import random
import datetime
import nats

# Authorization model for swagger
authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}

app = flask.Flask(__name__)
api = flask_restx.Api(app=app, doc='/doc',
                      title='API for CheckoutService', description='API for CheckoutService',
                      version="0.1", authorizations=authorizations, security='apikey', namespace="checkout_service")

# ENVIRONMENT VARIABLES
API_KEY = os.environ["API_KEY"]
NATS_HOST = os.environ["NATS_HOST"]
NATS_PORT = os.environ["NATS_PORT"]
CARTSERVICE_URL = os.environ["CARTSERVICE_URL"]
CARTSERVICE_API_KEY = os.environ["CARTSERVICE_API_KEY"]
CUSTOMERSSERVICE_URL = os.environ["CUSTOMERSSERVICE_URL"]
CUSTOMERSSERVICE_API_KEY = os.environ["CUSTOMERSSERVICE_API_KEY"]
PRODUCTCATALOGSERVICE_URL = os.environ["PRODUCTCATALOGSERVICE_URL"]
PRODUCTCATALOGSERVICE_API_KEY = os.environ["PRODUCTCATALOGSERVICE_API_KEY"]
PAYMENTSERVICE_URL = os.environ["PAYMENTSERVICE_URL"]
PAYMENTSERVICE_API_KEY = os.environ["PAYMENTSERVICE_API_KEY"]
LOG_LEVEL = os.environ["LOG_LEVEL"]

app.logger.setLevel(LOG_LEVEL)

# VARIABLES LOGS
VARIABLES = (f"API_KEY : {API_KEY} \n"
             f"NATS_HOST : {NATS_HOST} \n"
             f"NATS_PORT : {NATS_PORT} \n"
             f"CARTSERVICE_URL : {CARTSERVICE_URL} \n"
             f"CARTSERVICE_API_KEY : {CARTSERVICE_API_KEY}\n"
             f"CUSTOMERSSERVICE_URL : {CUSTOMERSSERVICE_URL} \n"
             f"CUSTOMERSSERVICE_API_KEY : {CUSTOMERSSERVICE_API_KEY} \n"
             f"PRODUCTCATALOGSERVICE_URL : {PRODUCTCATALOGSERVICE_URL} \n"
             f"PRODUCTCATALOGSERVICE_API_KEY : {PRODUCTCATALOGSERVICE_API_KEY} \n"
             f"PAYMENTSERVICE_URL : {PAYMENTSERVICE_URL} \n"
             f"PAYMENTSERVICE_API_KEY : {PAYMENTSERVICE_API_KEY} \n"
             f"LOG_LEVEL : {LOG_LEVEL}")

app.logger.info(f'ENVIRONMENT VARIABLES : \n{VARIABLES}')

# API models
consignee_model = api.model('consignee', {
    'name': flask_restx.fields.String,
    'address': flask_restx.fields.String,
    'city': flask_restx.fields.String,
    'postalzip': flask_restx.fields.String
})

credit_card_model = api.model('credit_card', {
    'owner': flask_restx.fields.String,
    'number': flask_restx.fields.String,
    'ccv': flask_restx.fields.Integer,
    'expiration_year': flask_restx.fields.Integer,
    'expiration_month': flask_restx.fields.Integer
})

payload_model = api.model('payload', {
    'customer_id': flask_restx.fields.Integer,
    'consignee': flask_restx.fields.Nested(consignee_model),
    'credit_card': flask_restx.fields.Nested(credit_card_model)
})

# Metrics
user_request_counter = Counter('checkoutservice_counter_requests', 'User request counter', ["cid"])


def generate_order_reference():
    """Generate a random order reference

    :return: The order reference
    """
    return str(random.randint(1000000000000, 9999999999999))


def api_key_required(func):
    """Decorator to check for API key

    :param func: The function to decorate
    :return: The decorated function
    """

    def wrapper(*args, **kwargs):
        key = flask.request.headers.get('X-API-KEY')

        if key == API_KEY:
            return func(*args, **kwargs)
        else:
            flask.current_app.logger.error(f'Incorrect or missing API key')
            flask.abort(401, "Incorrect or missing API key")

    return wrapper


@api.route('/order')
class Order(flask_restx.Resource):
    @api.doc(security='apikey')
    @api.response(201, 'Created')
    @api.response(401, 'Incorrect or missing API key')
    @api.response(404, 'Cart not found')
    @api.expect(payload_model)
    @api_key_required
    def post(self):
        """Get the order"""
        # STEP 0 : get the data from the request
        data = flask.request.json
        app.logger.debug(f"Data from POST on /order : {data}")

        user_request_counter.labels(data["customer_id"]).inc()

        # STEP 1 : Get the order from CartService
        app.logger.debug(f"Request to GET on /cart : {CARTSERVICE_URL + '/cart/' + str(data['customer_id'])}")

        r = requests.get(CARTSERVICE_URL + "/cart/" + str(data["customer_id"]),
                         headers={"X-API-KEY": CARTSERVICE_API_KEY})

        if r.status_code != 200:
            app.logger.error(f"Cart not found in CartService{r.status_code} : {r.json()}")
            flask.abort(404, "Cart not found")

        cart = r.json()
        app.logger.debug(f"Response from GET on /cart : {r.json()}")

        # STEP 2 : Get the customer information from CustomersService
        app.logger.debug(
            f"Request to GET on /customer : {CUSTOMERSSERVICE_URL + '/customer/' + str(data['customer_id'])}")

        r = requests.get(CUSTOMERSSERVICE_URL + "/customer/" + str(data["customer_id"]),
                         headers={"X-API-KEY": CUSTOMERSSERVICE_API_KEY})

        if r.status_code != 200:
            app.logger.error(f"Customer not found in CustomerService {r.status_code} : {r.json()}")
            flask.abort(404, "Cart not found")

        app.logger.debug(f"Response from GET on /customer : {r.json()}")
        customer = r.json()

        # STEP 3 : Create the order :
        # Generate the order reference
        order_reference = generate_order_reference()
        app.logger.debug(f"Generated order reference : {order_reference}")

        total_price = 0
        for elm in cart["items"]:
            app.logger.debug(
                f"Request to GET on /product : {PRODUCTCATALOGSERVICE_URL + '/product/' + str(elm['product_id'])}")

            r = requests.get(PRODUCTCATALOGSERVICE_URL + "/product/" + str(elm["product_id"]),
                             headers={"X-API-KEY": PRODUCTCATALOGSERVICE_API_KEY})
            if r.status_code != 200:
                app.logger.error(f"Product not found in ProductCatalogService {r.status_code} : {r.json()}")
                flask.abort(404, "Cart not found")
            price = r.json()["price"]
            total_price += elm["quantity"] * price
        order = {
            "order": {
                "reference": order_reference,
                "date": datetime.datetime.today().strftime('%Y-%m-%d')
            },
            "customer": customer,
            "items": cart["items"],
            "total_items": cart["total_items"],
            "total_price": total_price
        }
        # STEP 4: Ask to PaymentService to pay the order using the payment information
        payload = {
            "amount": total_price,
            "credit_card": data["credit_card"]
        }
        app.logger.debug(f"Payload to POST on /payment : {payload}")

        app.logger.debug(f"Request to POST on /payment : {PAYMENTSERVICE_URL + '/payment'}")

        r = requests.post(PAYMENTSERVICE_URL + "/payment", json=payload, headers={"X-API-KEY": PAYMENTSERVICE_API_KEY})
        if r.status_code != 200:
            app.logger.error(f"Payment failed in PaymentService {r.status_code} : {r.json()}")
            flask.abort(404, "Cart not found")

        # STEP 5: Send to EmailOrderService and ShippingService
        try:
            nats.send_order_to_nats(order)
        except Exception as e:
            app.logger.error(f"Error while sending order to NATS : {e}")
            flask.abort(404, "Cart not found")

        # STEP 6 : Empty the cart in CartService
        app.logger.debug(f"Request to DELETE on /cart : {CARTSERVICE_URL + '/cart/' + str(data['customer_id'])}")

        r = requests.delete(CARTSERVICE_URL + "/cart/" + str(data["customer_id"]),
                            headers={"X-API-KEY": CARTSERVICE_API_KEY})

        if r.status_code != 200:
            app.logger.error(f"Cart not found in CartService {r.status_code} : {r.json()}")
            flask.abort(404, "Cart not found")

        app.logger.debug(f"Order created : {order}")
        return "Created", 201


@app.route('/metrics')
def metrics():
    """Metrics endpoint"""
    return generate_latest(), 200


if __name__ == '__main__':
    app.run(debug=True, port=5000, host="0.0.0.0")
