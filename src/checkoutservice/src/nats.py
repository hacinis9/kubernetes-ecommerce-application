import pynats2 as pynats
import json
import os
import logging

NATS_HOST = os.environ["NATS_HOST"]
NATS_PORT = os.environ["NATS_PORT"]

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

logger.debug(f'NATS_HOST : {NATS_HOST}')
logger.debug(f'NATS_PORT : {NATS_PORT}')


def send_order_to_nats(order):
    """Send order to ShippingService

    :param order: The order
    :return: The response from the shipping service
    """
    with pynats.NATSClient(f"nats://{NATS_HOST}:{NATS_PORT}") as client:
        # Connect
        client.connect()
        logger.info(f'Connected to NATS server at {NATS_HOST}:{NATS_PORT}')

        payload = json.dumps(order)
        # Publish a message
        client.publish(subject="order", payload=payload)


if __name__ == '__main__':
    payload = {
        "order": {
            "reference": "9454387186616",
            "date": "2023-07-10"
        },
        "customer": {
            "name": "Jean Rault",
            "phone": "0385251275",
            "email": "jean.rault@aol.fr",
            "address": "86 quai Saint-Nicolas",
            "city": "Auxerre",
            "postalzip": "89000"
        },
        "items": [
            {
                "product_id": "OLJCESPC7Z",
                "name": "Sunglasses",
                "quantity": 2,
                "price": 19.0
            },
            {
                "product_id": "0PUK6V6EV0",
                "name": "Candle Holder",
                "quantity": 4,
                "price": 18.0
            }
        ],
        "total_items": 6,
        "total_price": 110.0
    }
    send_order_to_nats(payload)
