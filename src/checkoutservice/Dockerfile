# Use an official Python runtime as a parent image
FROM python:3.8-slim

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Make port 5000 available to the world outside this container
EXPOSE 5000

# Define environment variables
ENV API_KEY=your_api_key
ENV NATS_HOST=nats_host
ENV NATS_PORT=4222
ENV CARTSERVICE_URL=cart_service_url
ENV CARTSERVICE_API_KEY=cart_service_api_key
ENV CUSTOMERSSERVICE_URL=customers_service_url
ENV CUSTOMERSSERVICE_API_KEY=customers_service_api_key
ENV PRODUCTCATALOGSERVICE_URL=product_catalog_service_url
ENV PRODUCTCATALOGSERVICE_API_KEY=product_catalog_service_api_key
ENV PAYMENTSERVICE_URL=payment_service_url
ENV PAYMENTSERVICE_API_KEY=payment_service_api_key
ENV LOG_LEVEL=DEBUG

# Run app.py when the container launches
CMD ["python", "src/app.py"]
