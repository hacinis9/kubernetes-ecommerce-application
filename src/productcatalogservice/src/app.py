import os
import psycopg2
from flask import Flask, current_app, g, request
from flask_restx import Api, Resource, abort, fields
from prometheus_client import Counter, generate_latest

app = Flask(__name__)

authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}

api = Api(app, doc='/doc', authorizations=authorizations, security='apikey', title='Product Catalog Service API',
          description='This services provides access to all products in the catalog',
          namespace="product_catalog_service")

API_KEY = os.environ.get("API_KEY")
app.logger.setLevel(os.environ.get("LOG_LEVEL"))

app.logger.info({
    "API_KEY": os.environ.get("API_KEY"),
    "POSTGRES_USER": os.environ.get("POSTGRES_USER"),
    "POSTGRES_PASSWORD": os.environ.get("POSTGRES_PASSWORD"),
    "POSTGRES_HOST": os.environ.get("POSTGRES_HOST"),
    "POSTGRES_PORT": os.environ.get("POSTGRES_PORT"),
    "POSTGRES_DB": os.environ.get("POSTGRES_DB"),
    "LOG_LEVEL": os.environ.get("LOG_LEVEL")
})

# Prometheus counters

counter_number_of_searches_on_product = Counter('productcatalogservice_counter_of_searches_on_product',
                                                'The number of searches on a product', ['pid'])
counter_number_of_searches_on_unknown_product = Counter('productcatalogservice_counter_of_searches_on_unknown_product',
                                                        'The number of searches on an unknown product')

# API documentation models

categories_model = api.model('Categories', {
    'categories': fields.List(fields.String, description='List of product categories')
})

product_model = api.model('Product', {
    'id': fields.String(required=True, description='The product identifier'),
    'name': fields.String(required=True, description='The name of the product'),
    'description': fields.String(required=True, description='The description of the product'),
    'price': fields.Float(required=True, description='The price of the product'),
    'categories': fields.List(fields.String, description='List of product categories')
})

product_detail_model = api.model('ProductDetail', {
    'id': fields.String(required=True, description='The product identifier'),
    'name': fields.String(required=True, description='The name of the product'),
    'description': fields.String(required=True, description='The description of the product'),
    'price': fields.Float(required=True, description='The price of the product')
})


# Connecting to the database

def get_db():
    if 'db' not in g:
        g.db = psycopg2.connect(host=os.environ.get("POSTGRES_HOST"),
                                port=os.environ.get("POSTGRES_PORT", 5432),
                                database=os.environ.get("POSTGRES_DB"),
                                user=os.environ.get("POSTGRES_USER"),
                                password=os.environ.get("POSTGRES_PASSWORD"))
        current_app.logger.info(f'Connexion à la BDD')
    return g.db


def close_db():
    db = g.pop('db', None)
    if db is not None:
        db.close()
        current_app.logger.info(f'Déconnexion de la BDD')


# API KEY checking decorator

def api_key_required(func):
    def wrapper(*args, **kwargs):
        key = request.headers.get('X-API-KEY')

        if key == API_KEY:
            return func(*args, **kwargs)
        else:
            current_app.logger.error(f'Incorrect or missing API key')
            abort(401, "Incorrect or missing API key")

    return wrapper


# API endpoints
@api.route('/api/categories')
class Categories(Resource):
    """
    GET: Returns a list of all categories
    """

    @api.doc(security='apikey')
    @api.response(401, "Incorrect or missing API key")
    @api.response(200, "OK", categories_model)
    @api_key_required
    def get(self):
        """
        Get the list of product categories

        """
        db = get_db()
        cursor = db.cursor()
        cursor.execute("SELECT * FROM categories")
        row = cursor.fetchall()
        if row is not None:
            current_app.logger.info(f'Categories found in the database')
            list_of_categories = []
            for elm in row:
                list_of_categories.append(elm[0])
            return {"categories": list_of_categories}, 200

        current_app.logger.error(f'No categories were found in the database')
        return None, 404


@api.route('/api/products')
class Products(Resource):
    """
    GET: Returns the list of all the products
    """

    @api.doc(security='apikey')
    @api.response(401, "Incorrect or missing API key")
    @api.response(200, "OK", [product_model])
    @api_key_required
    def get(self):
        """
        Returns a list of all the products

        """
        db = get_db()
        cursor = db.cursor()
        cursor.execute("SELECT * FROM products INNER JOIN product_category ON products.pid = product_category.pid INNER JOIN categories ON product_category.cid = categories.cid")
        row = cursor.fetchall()
        if row is not None:
            current_app.logger.info(f'Products found in the database')
            list_of_products = []
            for elm in row:
                list_of_products.append({"id": elm[4], "name": elm[0], "description": elm[1], "price": elm[3],
                                         "categories": elm[7].split("|")})
            return list_of_products, 200

        current_app.logger.error(f'No products were found in the database')
        return None, 404


@api.param("product_id", "The ID of the product")
@api.route('/api/product/<product_id>')
class ProductId(Resource):
    """
    GET: Returns the caracteristics of a specific product
    """

    @api.doc(security='apikey')
    @api.response(404, "Product not found")
    @api.response(401, "Incorrect or missing API key")
    @api.response(200, "OK", product_detail_model)
    @api_key_required
    def get(self, product_id):
        """
        Returns info on a product by its ID

        """
        db = get_db()
        cursor = db.cursor()
        cursor.execute("SELECT * FROM products WHERE products.pid = %(product_id)s", {"product_id": product_id})
        row = cursor.fetchone()
        if row is not None:
            current_app.logger.info(f'The product {product_id} found in the database')
            counter_number_of_searches_on_product.labels(pid=row[4]).inc()
            return {
                "id": row[4],
                "name": row[0],
                "description": row[1],
                "price": row[3]
            }, 200

        current_app.logger.error(f'The product {product_id} was not found in the database')
        counter_number_of_searches_on_unknown_product.inc()
        return None, 404


@app.route('/api/products/metrics')
def metrics():
    return generate_latest()


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5000)
