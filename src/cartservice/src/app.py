import json
import os
import redis
from flask import Flask, current_app, request
from flask_restx import Api, Resource, abort, fields
from prometheus_client import Counter, generate_latest

app = Flask(__name__)

authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}

api = Api(app, doc='/doc', authorizations=authorizations, security='apikey', title='Cart Service API',
          description='This service provides access to carts', namespace="cart_service")
API_KEY = os.environ.get("API_KEY")

# Prometheus counters

counter_number_of_get_requests = Counter('cartservice_counter_get_requests',
                                         'The number of get requests on the cart', ["cid"])
counter_number_of_post_requests = Counter('cartservice_counter_post_requests',
                                          'The number of post requests on the cart', ["cid"])
counter_number_of_delete_requests = Counter('cartservice_counter_delete_requests',
                                            'The number of delete requests on the cart', ["cid"])


# API models

payload_model = api.model('Payload', {
    'product_id': fields.Integer(required=True, description='The product identifier'),
    'quantity': fields.Integer(required=True, description='Quantity to add')
})

cart_item_model = api.model('CartItem', {
    'product_id': fields.Integer(description='The product identifier'),
    'quantity': fields.Integer(description='Quantity of the product in the cart')
})

cart_model = api.model('Cart', {
    'items': fields.List(fields.Nested(cart_item_model)),
    'total_items': fields.Integer(description='Total number of items in the cart')
})


# Connecting to the database

def get_db():
    r = redis.Redis(host=os.environ.get("DB_HOST"), port=os.environ.get("DB_PORT", 6379),
                    db=os.environ.get("DB_NUM", 0))
    current_app.logger.info(f'Connected to the database')
    return r


# API KEY checking decorator
def api_key_required(func):
    def wrapper(*args, **kwargs):
        key = request.headers.get('X-API-KEY')

        if key == API_KEY:
            return func(*args, **kwargs)
        else:
            current_app.logger.error(f'Incorrect or missing API key')
            abort(401, "Incorrect or missing API key")

    return wrapper


# API endpoints
@api.route('/cart/<customer_id>')
class Cart(Resource):
    """
    GET: Returns the content of a customer's cart
    POST: Adds a product to a customer's cart
    DELETE: Removes a customer's cart
    """

    @api.doc(security='apikey')
    @api.response(404, "Cart not found")
    @api.response(200, "OK", cart_model)
    @api.response(401, "Incorrect or missing API key")
    @api_key_required
    def get(self, customer_id):
        """
        Returns the content of a customer's cart

        """
        counter_number_of_get_requests.labels(cid=customer_id).inc()
        db = get_db()
        value = db.get(customer_id)
        if value is not None:
            current_app.logger.info(f'Cart found in the database')
            value = json.loads(value.decode('utf-8'))
            return value, 200

        current_app.logger.error(f'Cart not found in the database')
        return 404

    @api.doc(security='apikey')
    @api.expect(payload_model)
    @api.response(404, "Cart not found")
    @api.response(201, "Added")
    @api.response(401, "Incorrect or missing API key")
    @api_key_required
    def post(self, customer_id):
        """
        Adds a product to a customer's cart

        """
        counter_number_of_post_requests.labels(cid=customer_id).inc()
        db = get_db()
        cart = db.get(customer_id)
        if cart is not None:
            cart = json.loads(cart.decode('utf-8'))
            current_app.logger.info(f'Retrieved cart content for customer : {cart}')
            # Updating the cart's content
            cartids = []
            for elm in cart["items"]:
                cartids.append(elm["product_id"])
            if request.json["product_id"] in cartids:
                for elm in cart["items"]:
                    if elm["product_id"] == request.json["product_id"]:
                        elm["quantity"] = elm["quantity"] + request.json["quantity"]
            else:
                cart["items"].append(request.json)
            cart["total_items"] = cart["total_items"] + request.json["quantity"]
            current_app.logger.info(f'Cart content has been updated for customer')
            add = db.set(customer_id, json.dumps(cart))
            if add:
                current_app.logger.info(f'Item added in the cart')
                return 201
        else:
            current_app.logger.info(f'Cart not found in the database')
            cart = {"items": [], "total_items": 0}
            current_app.logger.info(f'Created the cart for customer')
            cart["items"].append(request.json)
            cart["total_items"] = cart["total_items"] + request.json["quantity"]
            current_app.logger.info(f'Cart content has been added for customer')
            add = db.set(customer_id, json.dumps(cart))
            if add:
                current_app.logger.info(f'Item added in the cart')
                return 201

    @api.doc(security='apikey')
    @api.response(404, "Cart not found")
    @api.response(200, "OK")
    @api.response(401, "Incorrect or missing API key")
    @api_key_required
    def delete(self, customer_id):
        """
        Deletes the cart of a customer

        """
        counter_number_of_delete_requests.labels(cid=customer_id).inc()
        db = get_db()
        cart = db.get(customer_id)
        if cart is not None:
            current_app.logger.info(f'Cart found in the database')
            db.delete(customer_id)
            current_app.logger.info(f'Cart for customer has been removed')
            return 200
        else:
            current_app.logger.info(f'Cart not found in the database')
            return 404


@app.route('/metrics')
def metrics():
    return generate_latest()


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5000)
